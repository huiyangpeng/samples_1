
|  目录名称                                                   |  功能描述                                              |  运行环境 |
| ------------------------------------------------------------ | ---------------------------------------------------- | -- |
| [AddcdivCustomSample](./AddcdivCustomSample) | 基于Ascend C的Addcdiv自定义Vector算子及调用样例 | Atlas训练系列产品<br>Atlas推理系列产品(Ascend 310P 处理器)<br>Atlas A2训练系列产品 |
| [AddCustomSample](./AddCustomSample) | 基于Ascend C的Add自定义Vector算子及调用样例 | Atlas训练系列产品<br>Atlas 200/500 A2 推理产品<br>Atlas推理系列产品(Ascend 310P 处理器)<br>Atlas A2训练系列产品 |
| [HelloWorldSample](./HelloWorldSample) | 基于Ascend C的自定义算子调用结构演示样例 |Atlas训练系列产品<br>Atlas推理系列产品(Ascend 310P 处理器)<br>Atlas A2训练系列产品 |
| [LeakyReluCustomSample](./LeakyReluCustomSample) | 基于AscendC的LeakyReLU自定义Vector算子及调用样例 |Atlas推理系列产品(Ascend 310P 处理器)<br>Atlas 200/500 A2 推理产品<br>Atlas A2训练系列产品|
| [LayerNormCustomSample](./LayerNormCustomSample) | 基于AscendC的LayerNorm自定义算子及调用样例 | Atlas A2训练系列产品 |
| [MatMulCustomSample](./MatMulCustomSample) | 基于AscendC的Matmul自定义Cube算子及调用样例 | Atlas推理系列产品(Ascend 310P 处理器)<br>Atlas A2训练系列产品 |
| [MatMulLeakyReluCustomSample](./MatMulLeakyReluCustomSample) | 基于AscendC的MatMulLeakyRelu自定义Cube+Cube算子及调用样例 | Atlas推理系列产品(Ascend 310P 处理器)<br>Atlas A2训练系列产品 |
| [MoeSoftMaxTopkCustomSample](./MoeSoftMaxTopkCustomSample) | 基于AscendC的MoeSoftMaxTopk自定义算子及调用样例 |  Atlas A2训练系列产品 |
| [PreLayerNormSample](./PreLayerNormSample) | 基于AscendC的PreLayerNorm自定义算子及调用样例 |  Atlas A2训练系列产品 |
| [SubCustomSample](./SubCustomSample) | 基于AscendC的Sub自定义算子及调用样例 |Atlas训练系列产品<br>Atlas推理系列产品(Ascend 310P 处理器)<br>Atlas A2训练系列产品 |
