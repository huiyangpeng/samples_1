 #include <stdint.h>
#ifndef ADD_CUSTOM_TILING_H
#define ADD_CUSTOM_TILING_H

struct AddCustomTilingData
{
    uint32_t totalLength;
    uint32_t tileNum;
};
#endif