
| 目录名称                                                   | 功能描述                                             |
| ------------------------------------------------------------ | ---------------------------------------------------- |
| [ACLHelloWorld](./ACLHelloWorld) | HelloWord for AscendCL, 一个简单样例快速理解AscendCL基础概念 |
| [memoryManagement](./memoryManagement) | 内存管理样例 |
| [mediaProcess](./mediaProcess) | 媒体数据处理样例 |
| [modelInference](./modelInference) | 模型推理样例 |
| [moreModelFeatures](./moreModelFeatures) | 使用更多特性的模型推理样例 |
| [contributeSamples](./contributeSamples) | 贡献样例 |
| [acllite](./acllite) | acllite统一封装接口 |